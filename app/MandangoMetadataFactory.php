<?php

class MandangoMetadataFactory extends \Mandango\MetadataFactory
{
    protected $classes = array(
        'Blogger\\BlogBundle\\Model\\Enquiry' => false,
        'Blogger\\BlogBundle\\Model\\Article' => false,
    );
}